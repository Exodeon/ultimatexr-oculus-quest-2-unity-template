# ultimatexr-oculus-quest-2-unity-template

This is a Unity template for VR projects for Oculus Quest 2, using the Ultimate XR template.
This is intended to use with Unity 2021.3.15f1.

## How to use it
- Put the `package` folder in a `tgz` archive, with the same name that the name specified in `package.json` and the version. At the moment, this should be `com.unity.template.uxrquest2-1.0.0.tgz`. 
   - To do it with 7-Zip on Windows : right click on the folder, and go to 7-Zip > Add to archive. Put the given filename, but start with a `tar` compression.
   - Right click on the given `tar` archive, and compress it again with 7-Zip. This time, you should have `gzip` compression, and you can put the `tgz` extension.  

- Now, put the `tgz` in the templates folder. This should be something like `C:\Program Files\Unity\Hub\Editor\2021.3.15f1\Editor\Data\Resources\PackageManager\ProjectTemplates`.
- Restart Unity Hub and try to create a project, the template should appear.

When the project is created, you will need to go to build settings and switch platform to Android with ASTC textures compression. Then the project should be ready to use (you can try with the UltimateXR example scene).

